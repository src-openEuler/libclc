%bcond_without check
%bcond_without sys_llvm
%bcond_without bisheng_autotuner

%global maj_ver 17
%global min_ver 0
%global patch_ver 6
%global clc_version %{maj_ver}.%{min_ver}.%{patch_ver}
%global clc_srcdir libclc-%{clc_version}.src
%global debug_package %{nil}

%if %{with sys_llvm}
%global pkg_name libclc
%global install_prefix %{_prefix}
%else
%global pkg_name libclc%{maj_ver}
%global install_prefix %{_libdir}/llvm%{maj_ver}
%endif

%if 0%{?__isa_bits} == 64
%global install_libdir %{install_prefix}/lib64
%else
%global install_libdir %{install_prefix}/lib
%endif
%global install_includedir %{install_prefix}/include

Name:           %{pkg_name}
Version:        %{clc_version}
Release:        4
Summary:        An implementation of the library requirements of the OpenCL C
License:        Apache-2.0 WITH LLVM-exception OR BSD OR MIT
URL:            https://libclc.llvm.org
Source0:        https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/%{clc_srcdir}.tar.xz

ExclusiveArch:  %{ix86} x86_64 %{arm} aarch64 riscv64 loongarch64 ppc64le
BuildRequires:  clang
BuildRequires:  clang-devel >= %{version}
BuildRequires:	libedit-devel
%if %{with sys_llvm}
BuildRequires:  llvm-devel = %{version}
%else
BuildRequires:	llvm%{maj_ver}-devel = %{version}
%endif
BuildRequires: 	python3
BuildRequires: 	zlib-devel
BuildRequires:  cmake
BuildRequires:	spirv-llvm-translator-tools

%description
libclc is an open source, BSD/MIT dual licensed implementation of the
library requirements of the OpenCL C programming language, as
specified by the OpenCL 1.1 Specification. The following sections
of the specification impose library requirements:

  * 6.1: Supported Data Types
  * 6.2.3: Explicit Conversions
  * 6.2.4.2: Reinterpreting Types Using as_type() and as_typen()
  * 6.9: Preprocessor Directives and Macros
  * 6.11: Built-in Functions
  * 9.3: Double Precision Floating-Point
  * 9.4: 64-bit Atomics
  * 9.5: Writing to 3D image memory objects
  * 9.6: Half Precision Floating-Point

libclc is intended to be used with the Clang compiler's OpenCL frontend.

libclc is designed to be portable and extensible. To this end, it
provides generic implementations of most library requirements,
allowing the target to override the generic implementation at the
granularity of individual functions.

libclc currently supports the AMDGCN, and R600 and NVPTX targets, but
support for more targets is welcome.

%package        devel
Summary:        Development files for libclc
Requires:       %{name} = %{version}-%{release}

%description    devel
The libclc-devel package contains libraries and header files for
developing applications that use libclc.

%prep
%autosetup -n %{name}-%{version}.src -p1

%build
export CFLAGS="%{build_cflags} -D__extern_always_inline=inline"
%cmake \
%if %{with bisheng_autotuner}
	-DLLVM_ENABLE_AUTOTUNER=ON \
%endif
  -DCMAKE_INSTALL_DATADIR:PATH=%{install_libdir}

%cmake_build

%install
%cmake_install

%if %{with check}
%check
%cmake_build --target test || true
%endif

%files
%license LICENSE.TXT
%doc README.TXT CREDITS.TXT
%dir %{install_libdir}/clc
%{install_libdir}/clc/*.bc
%{install_libdir}/clc/spirv-mesa3d-.spv
%{install_libdir}/clc/spirv64-mesa3d-.spv
%{install_includedir}/clc

%files devel
%{install_libdir}/pkgconfig/%{name}.pc
%{install_includedir}/clc

%changelog
* Wed Nov 13 2024 Funda Wang <fundawang@yeah.net> - 17.0.6-4
- adopt to new cmake macro

* Tue Aug 20 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-3
- Add BiSheng Autotuner support.

* Thu Mar 14 2024 yinxiulin <xiulin.yin@shingroup.cn> - 17.0.6-2
- Add ppc64le support

* Mon Dec 4 2023 zhoujing <zhoujing106@huawei.com> - 17.0.6-1
- Update to 17.0.6

* Sat Aug 05 2023 cf-zhao <zhaochuanfeng@huawei.com> - 15.0.7-2
- Disable check temporarily due to two failed cases

* Fri Jun 23 2023 cf-zhao <zhaochuanfeng@huawei.com> - 15.0.7-1
- Update to 15.0.7

* Thu Feb 16 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 12.0.1-2
- Add loongarch64 support

* Fri Mar 18 2022 yaoxin <yaoxin30@huawei.com> - 12.0.1-1
- Upgrade libclc to 12.0.1 to resolve compilation failures.

* Tue Aug 11 2020 yanan li <liyanan032@huawei.com> -0.2.0-16
- Modify python  to python3

* Thu Jul 09 2020 whoisxxx <zhangxuzhou4@huawei.com> - 0.2.0-15
- Add RISC-V arch

* Tue Dec 31 2019 Jiangping Hu <hujiangping@huawei.com> - 0.2.0-14
- Package init
